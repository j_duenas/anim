File Name: Animation Maker
Version: 1.0
For: TI-83+
Programmed By: Jeremy Duenas
Description:

This program allows you to make simple little animations. Just choose how many character sets you want. Then enter up to 16 characters for each set. Then watch your animation go!


Contact Information:

Email- ToxicityJ@gmail.com
AIM- Duenos14
Yahoo- Toxicity2788
Website- www.toxicityj.bravehost.com
